# Configure AWS Lambda parser

## Setup environment using virtualenv (Python 3.6)

- Install requirements

  - `pip install -r requirements.txt`

## Configure AWS

### All services must be in the same region

- Create IAM user
    1. Open **IAM Management Console**
    2. Create new user with **Programmatice access** access type
    3. Create new group for the user with permissions:

        - **AmazonRDSFullAccess**
        - **AWSLambdaFullAccess**
        - **IAMFullAccess**
        - **AmazonS3FullAccess**
        - **AmazonAPIGatewayAdministrator**
        - **AWSCloudFormationReadOnlyAccess**
    4. Create **Inline Policy** for the group

        - On the group **Permissions** tab at the bottom click on the **Create Group Policy** button
        - Select **Custom Policy**
        - Enter Policy name and insert this in the **Policy Document** field:
            ```json
            {
                "Version": "2012-10-17",
                "Statement": [
                    {
                        "Sid": "Stmt1449904348000",
                        "Effect": "Allow",
                        "Action": [
                            "lambda:InvokeAsync",
                            "lambda:InvokeFunction",
                            "cloudformation:CreateStack",
                            "cloudformation:UpdateStack",
                            "cloudformation:DeleteStack",
                            "rds:DescribeDBInstances",
                            "s3:DeleteObject",
                            "s3:GetObject",
                            "s3:PutObject",
                            "s3:CreateMultipartUpload",
                            "s3:AbortMultipartUpload",
                            "s3:ListMultipartUploadParts",
                            "s3:ListBucketMultipartUploads",
                            "iam:ListRoles",
                            "iam:CreateRole",
                            "iam:CreatePolicy",
                            "iam:AttachRolePolicy",
                            "s3:ListBucket",
                            "s3:ListObjects",
                            "kms:ListKeys"
                        ],
                        "Resource": [
                            "*"
                        ]
                    }
                ]
            }
            ```
        - Click **Apply Policy** button
        - Save user's **Access key ID** and **Secret access key**
- Create S3 bucket for **Django** static files
- Create VPC

  1. Open **VPC Dashboard**
  2. Click **Create VPC** button
  3. Choose **VPC with Public and Private Subnets**
  4. Enter VPC name
  5. Select **Aviabilty zone** if you need
  6. Choose **Elastic IP Allocation ID**
  7. Create VPC
  8. Create additional private subnets for all availabilty zone
  9. Go to **Route Tables**, find route table pointing to **NAT** and association to the just created subnets

- Create RDS instance

  1. Open RDS service
  2. Click **Launch a DB instance**
  3. Select **MySQL**
  4. Select **Dev/Test - MySQL**
  5. In **Instance specification** section select **small** DB instance class
  6. In **Settings** section enter **DB instance identifier**, **Master username** and **Master password**. Click Next
  7. Select VPC you have created previously
  8. Set **Public accessibility** to **Yes**
  9. Enter Database name
  10. Click **Launch DB Instance**
  11. Wait until status of the db become **available**
  12. Open instance detail page
  13. In the **Connect** section open **Security group** with **CIDR/IP - Inbound** type
  14. In the Security group detail page at the bottom select **Inbound** tab
  15. Click **Edit**
  16. Add **MySQL/Aurora** rule and set **Anywhere** source.

## Project configuration

1. Edit **zappa_settings.json** file and enter your data
    - **aws region**
    - **project_name**
    - **s3_bucket** name (Zappa will create the bucket)
    - in **SubnetIds** enter your **private** subnets
    - in **SecurityGroupIds** enter your VPC security group id
2. Edit **DATABASES** in the Django settings file
    - Enter appropriate **HOST**, **NAME**, **USER** and **PASSWORD**
        - **HOST** you can find as **Endpoint** in your DB instance detail page, in the **Connect** section
3. Edit AWS constanst
    - **AWS_ACCESS_KEY_ID**
    - **AWS_SECRET_ACCESS_KEY**
    - **AWS_STORAGE_BUCKET_NAME** (S3 bucket name you have created previously)
4. Run `zappa deploy dev` command
5. Copy your lambda URL (Zappa will print it to the console) and paste it to **ALLOWED_HOSTS** in the **product_parser/settings.py** file
6. Run `zappa update` command
7. Run `zappa manage dev "collectstatic --noinput"`
8. Open your lambda URL and navigate to /admin. Check if static files are loading
9. Run `zappa manage dev migrate` command
    - It will apply migrations to the DB
10. Run `zappa invoke --raw dev "from django.contrib.auth.models import User; User.objects.create_superuser('admin', 'admin@admin.com', 'qweqweqwe')"` command
    - It will create a superuser. You can change username, email and password in this part **User.objects.create_superuser('admin', 'admin@admin.com', 'qweqweqwe')**

## Create EC2 instance to get access to the DB

1. Go to EC2 Dashboard
2. Click **Launch Instance**
3. Select **Ubuntu Server**
4. Choose Instance type and click **Next: Configure Instance Details**
5. In **Network** select your VPC
6. In **Subnet** select on of the private subnets
7. in **Auto-assign Public IP** select **Enable**
8. Keep clicking **Next** until step 6
9. You already have rule for SSH connection, just change **Source** to **Anywhere**
10. Click **Review and Launch**
11. Click **Launch**
12. Choose existing keys or create new
13. Connect to the instance via SSH
14. Copy db dump to your instance using **scp**
15. Install mysql client
    - `sudo apt install mysql-client`
16. Apply dump using mysql cli

## Last steps

1. Run `zappa manage dev "migrate --fake-initial"` command
2. Go to `/parse` url for run parser
