from time import sleep

from django.db import connection
from django.utils import timezone
from zappa.async import task

from .models import Product
from .utils import get_text
from .webdriver import WebDriver

TESCO = 1
ALDI = 2
ASDA = 3
ICELAND = 4
MORRISONS = 5
OCADO = 6
SAINSBURYS = 7
WAITROSE = 8

# Initiate Chrome headless
driver = WebDriver()


def parse_retaielrs():
    parse_retailer(TESCO)
    parse_retailer(ALDI)
    parse_retailer(ASDA)
    parse_retailer(ICELAND)
    parse_retailer(MORRISONS)
    parse_retailer(OCADO)
    parse_retailer(SAINSBURYS)
    parse_retailer(WAITROSE)


@task
def parse_retailer(retailer_id):
    products = Product.objects.filter(
        retailer_id=retailer_id
    ).values('id')

    for product in products:
        if retailer_id == WAITROSE:
            sleep(1)

        handle_product(product.get('id'), retailer_id)


@task
def handle_product(product_id, retailer_id):
    product = Product.objects.get(pk=product_id)

    url = product.product_url

    if url:
        driver.get(url)

        if retailer_id == TESCO:
            # Tesco

            price_wrapper = driver.get_element_by_class_name(
                'price-control-wrapper'
            )
        elif retailer_id == ALDI:
            # Aldi

            price_wrapper = driver.get_element_by_class_name(
                'product-price__main'
            )

        elif retailer_id == ASDA:
            # Asda

            price_wrapper = driver.get_element_by_class_name(
                'prod-price-inner'
            )
        elif retailer_id == ICELAND:
            # Iceland

            price_wrapper = driver.get_element_by_xpath(
                "//div[@class='detailPriceContainer cf left ']/span[@class='big-price fontForty left block bold mBsevenPix']"
            )
        elif retailer_id == MORRISONS:
            # Morrisons
            price_wrapper = driver.get_element_by_xpath(
                "//div[@class='typicalPrice']/h5"
            )
        elif retailer_id == OCADO:
            # Ocado

            price_wrapper = driver.get_element_by_xpath(
                "//div[@class='typicalPrice']/h5"
            )
        elif retailer_id == SAINSBURYS:
            # Sainsburys

            price_wrapper = driver.get_element_by_xpath(
                "//div[@class='pricing']/p[@class='pricePerUnit']"
            )
        elif retailer_id == WAITROSE:
            # Waitrowse

            price_wrapper = driver.get_element_by_xpath(
                "//span[@data-test='product-pod-price']"
            )

        price = get_text(price_wrapper)

        if retailer_id == SAINSBURYS and price:
            price = price.split('/')[0]

        if retailer_id == WAITROSE and 'eachest.' in price:
            price = price.replace('eachest.', '')

        if price:
            product.product_price = price

    product.updated = timezone.now()
    product.save()
    connection.close()
