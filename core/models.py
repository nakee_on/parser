from django.db import models


class Product(models.Model):
    product_url = models.CharField(max_length=2048, null=True)
    image_url = models.CharField(max_length=2048, null=True)
    product_name = models.CharField(max_length=200, null=True)
    product_price = models.CharField(max_length=255, null=True)
    file = models.CharField(max_length=250, null=True)
    updated = models.DateTimeField(null=True)
    file_db = models.CharField(max_length=250, null=True)
    retailer_id = models.IntegerField(null=True)

    class Meta:
        db_table = 'product'

    def __str__(self):
        return self.product_name
