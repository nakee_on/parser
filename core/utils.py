def get_text(element):
    """ Returns text inside element """

    if element:
        return element.text.replace(' ', '')

    return None
