from django.conf import settings
from selenium.common.exceptions import TimeoutException
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait


class WebDriver:
    def __init__(self):
        chrome_options = Options()
        chrome_options.add_argument("--headless")
        chrome_options.add_argument("--no-sandbox")
        chrome_options.add_argument("--disable-gpu")
        chrome_options.add_argument("--disable-dev-shm-usage")
        chrome_options.add_argument("--disable-extensions")
        chrome_options.add_argument('--window-size=1280x1696')
        chrome_options.add_argument('--hide-scrollbars')
        chrome_options.add_argument('--single-process')
        chrome_options.add_argument('--ignore-certificate-errors')
        chrome_options.add_argument('--homedir=/tmp')
        chrome_options.add_argument('--disable-logging')
        chrome_options.binary_location = settings.CHROME_BINARY

        self._driver = webdriver.Chrome(
                executable_path=settings.CHROME_DRIVER,
                chrome_options=chrome_options
        )

    def get_element_by_class_name(self, class_name):
        try:
            element = WebDriverWait(self._driver, 5).until(
                EC.presence_of_element_located((By.CLASS_NAME, class_name))
            )
        except TimeoutException:
            return None

        return element

    def get_element_by_xpath(self, xpath):
        try:
            element = WebDriverWait(self._driver, 5).until(
                EC.presence_of_element_located((By.XPATH, xpath))
            )
        except TimeoutException:
            return None

        return element

    def get(self, url):
        self._driver.get(url)
