import MySQLdb
from django.core.management.base import BaseCommand, CommandError
from django.conf import settings

class Command(BaseCommand):
    help = 'Creates the initial database'

    def handle(self, *args, **options):
        self.stdout.write(self.style.SUCCESS('Starting db creation'))

        dbname = settings.DATABASES['default']['NAME']
        user = settings.DATABASES['default']['USER']
        password = settings.DATABASES['default']['PASSWORD']
        host = settings.DATABASES['default']['HOST']

        db = MySQLdb.connect(user=user, host=host, password=password)
        cur = db.cursor()
        cur.execute("CREATE DATABASE parser;")
        cur.execute("GRANT ALL PRIVILEGES ON parser.* TO parser")
        cur.close()
        db.close()

        self.stdout.write(self.style.SUCCESS('All Done'))
