from django.contrib import admin
from django.urls import path
from core.views import parse_retailer

urlpatterns = [
    path('admin/', admin.site.urls),
    path('parse/', parse_retailer),
]
